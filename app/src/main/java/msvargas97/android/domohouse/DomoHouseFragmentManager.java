/*
 * Michael Vargas Copyright (c) 2017. - Colombia
 */

package msvargas97.android.domohouse;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * * Created by Michael Vargas on 23/07/2017 11:05 PM
 * File:msvargas97.android.domohouse
 */

public class DomoHouseFragmentManager {
    private FragmentManager fm;
    private Fragment[] fragments = null;
    private int currentFragment,lastFragment,containerView;
    public DomoHouseFragmentManager(FragmentManager fm, Fragment[] fragments,int containerView) {
        this.fm = fm;
        this.fragments = fragments;
        this.containerView = containerView;
    }
    public void setFragment(int numFragment) {
        try {
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(containerView, fragments[numFragment]);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            lastFragment = currentFragment;
            currentFragment = numFragment;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Fragment[] getFragments() {
        return fragments;
    }

    public void setFragments(Fragment[] fragments) {
        this.fragments = fragments;
    }

    public int getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment(int currentFragment) {
        this.currentFragment = currentFragment;
    }

    public int getLastFragment() {
        return lastFragment;
    }

    public void setLastFragment(int lastFragment) {
        this.lastFragment = lastFragment;
    }
    public void setTransitionFragment(int numFragment){
        lastFragment = currentFragment;
        currentFragment = numFragment;
    }
}
