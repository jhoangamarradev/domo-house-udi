/*
 * Michael Vargas Copyright (c) 2017. - Colombia
 */

package msvargas97.android.domohouse;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;

import java.util.Calendar;

/**
 * * Created by Michael Vargas on 26/07/2017 3:57 PM
 * File:msvargas97.android.domohouse
 */

public class TimeDialog extends DialogFragment {
    private TimePickerDialog.OnTimeSetListener listener;
    private int hour,minute;


    public TimeDialog(TimePickerDialog.OnTimeSetListener listener){
        super();
        this.listener = listener;
        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
    }
    public TimeDialog(TimePickerDialog.OnTimeSetListener listener,int hour,int min){
        super();
        this.listener = listener;
        this.hour = hour;
        this.minute = min;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Iniciar con el tiempo actual

        // Retornar en nueva instancia del dialogo selector de tiempo
        return new TimePickerDialog(
                getActivity(),
                listener,
                hour,
                minute,
                true);
    }
}
