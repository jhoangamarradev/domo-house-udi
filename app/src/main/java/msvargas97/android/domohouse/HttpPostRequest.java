package msvargas97.android.domohouse;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * * Created by Michael Vargas on 24/07/2017 12:12 AM
 * File:msvargas97.android.domohouse
 */

public class HttpPostRequest extends AsyncTask<String, Integer, String>{
    ProgressDialog progressDialog;
    boolean enableProgress;
    String info,actionName;
    private Context context;
    private Activity activity;
    private boolean cancel = false;

    HttpPostRequest(Activity activity,boolean enableProgressBar,String info){
        this.enableProgress = enableProgressBar;
        this.info = info;
        this.activity = activity;
        context = activity.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(enableProgress && context != null){
            progressDialog = ProgressDialog.show(activity, "",info);  //show a progress dialog
            progressDialog.setCancelable(true);
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                HttpPostRequest.super.onCancelled(DomoHouseConstants.SERVER_RESULT);
                }
            });
        }
    }

    @Override
    protected String doInBackground(String... params) {
        BufferedReader in = null;
        String baseUrl = params[0];
        String jsonData = params[1];
        actionName = params[2];
        try {
            //Creamos un objeto Cliente HTTP para manejar la peticion al servidor
            HttpClient httpClient = new DefaultHttpClient();
           //  HttpURLConnection conn=(HttpURLConnection) httpClient.getConnectionManager();
           // conn.setConnectTimeout(60000); // timing out in a minute
            //Creamos objeto para armar peticion de tipo HTTP POST
            HttpPost post = new HttpPost(baseUrl);
            //Configuramos los parametros que vamos a enviar con la peticion HTTP POST
            List<NameValuePair> nvp = new ArrayList<NameValuePair>(2);
            nvp.add(new BasicNameValuePair(actionName, jsonData));
            post.setEntity(new UrlEncodedFormEntity(nvp));
            //Se ejecuta el envio de la peticion y se espera la respuesta de la misma.
            HttpResponse response = httpClient.execute(post);
            //Log.d(DomoHouseConstants.TAG, "Respuesta Raspberry Server"+response.getStatusLine().toString());
            //Obtengo el contenido de la respuesta en formato InputStream Buffer y la paso a formato String
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            Log.d(DomoHouseConstants.TAG,"Running HTTPostRequest");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            if(enableProgress) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return DomoHouseConstants.MSG_ERROR_SERVER;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    protected void onProgressUpdate(Integer... progress) {
        //Se obtiene el progreso de la peticion
        Log.w(DomoHouseConstants.TAG,"Indicador de progreso " + progress[0].toString());
    }

    @Override
    protected void onCancelled() {
        cancel = true;
        super.onCancelled();
        if(enableProgress && context != null)progressDialog.dismiss();
    }

    protected void onPostExecute(String result) {
    //Envia el resultado al MainActivity mediante un Intent
        if(enableProgress&& context != null)progressDialog.dismiss();
        if(context != null){
            if(cancel){
                result = DomoHouseConstants.MSG_ERROR_SERVER;
            }
            Intent intent = new Intent(context,MainActivity.class);
            intent.putExtra(DomoHouseConstants.SERVER_REQUEST,actionName.trim());
            intent.putExtra(DomoHouseConstants.SERVER_RESULT,result.trim());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);
        }
    }

}
