/*
 * Michael Vargas Copyright (c) 2017. - Colombia
 */

package msvargas97.android.domohouse;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * * Created by Michael Vargas on 27/07/2017 10:25 PM
 * File:msvargas97.android.domohouse
 */

public class ScenarieAdapter extends BaseAdapter {
    private static LayoutInflater inflater=null;
    private ArrayList<String>  names=new ArrayList<>(),
            hoursOn=new ArrayList<>(),
            hoursOff=new ArrayList<>(),
            dateBegin=new ArrayList<>(),
            dateEnd=new ArrayList<>(),
            timers = new ArrayList<>(),
            dow = new ArrayList<>();
    private ArrayList<String> listDelete = new ArrayList<>();
    private ArrayList<Boolean[][]> statesSelect = new ArrayList<>();
    private ArrayList<String[]> macros = new ArrayList<>();
    private Holder holder = null;
    private boolean stateCheckBox = false;
    private Context context;
    private int nLuces,nVentanas,nElectrodomesticos,nPuertas;
    //Clases internas - innerClasses
    private class Holder{
        TextView tvName,tvHoursOn,tvHoursOff;
        CheckBox checkBox;
    }
    public static class Scenarie{
        String name,hourOn,hourOff,dateBegin,dateEnd,timer,dow;
        String[] macros = new String[3];
        boolean [][] stateSaveSelect = new boolean[4][16];
    }
    ScenarieAdapter(Context context){
        this.context = context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    ScenarieAdapter(Context context,ArrayList<String> names,ArrayList<String> hoursOn,ArrayList<String> hoursOff,ArrayList<String> dateBegin,ArrayList<String> dateEnd){
        this.context = context;
        this.names = names;
        this.hoursOff = hoursOff;
        this.hoursOn = hoursOn;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    int indexOf(String name){
        if(getCount() > 0){
            return  names.indexOf(name);
        }else{
            return -1;
        }
    }
    void setArrays(ArrayList<String> names,ArrayList<String> hoursOn,ArrayList<String> hoursOff,ArrayList<String> dateBegin,ArrayList<String> dateEnd){
        this.names = names;
        this.hoursOff = hoursOff;
        this.hoursOn = hoursOn;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        notifyDataSetChanged();
    }
    void setNumElements(int nLuces,int nElectrodomesticos,int nVentanas,int nPuertas){
        this.nLuces = nLuces;
        this.nVentanas = nVentanas;
        this.nElectrodomesticos = nElectrodomesticos;
        this.nPuertas = nPuertas;
    }
    private Boolean[][] toPrimitiveBoolean( boolean[][] booleanVect) {
        final Boolean[][] primitives = new Boolean[4][16];
        for (int i = 0; i < 4;i++){
            for(int j = 0; j  < booleanVect[0].length;j++) {
                primitives[i][j] = booleanVect[i][j];
            }
        }
        return primitives;
    }
    private boolean[][] toPrimitiveBooleanArray( Boolean[][] booleanArray) {
        final boolean[][] primitives = new boolean[4][booleanArray[0].length];
        for (int i = 0; i < 4;i++){
            for(int j = 0; j  < booleanArray[0].length;j++) {
                primitives[i][j] = booleanArray[i][j];
            }
        }
        return primitives;
    }
    boolean add(String name,String dow,String hoursOn,String hourOff,String dateBegin,String dateEnd,boolean[][] statesChecksDevices){
        this.names.add(name);
        this.hoursOn.add(hoursOn);
        this.hoursOff.add(hourOff);
        this.dateBegin.add(dateBegin);
        this.dateEnd.add(dateEnd);
        this.dow.add(dow);
        this.statesSelect.add(toPrimitiveBoolean(statesChecksDevices));
        String [] schedule = addTimer(dow,dateBegin,dateEnd,hoursOn,hourOff,timers.size());
        this.timers.add(schedule[0]);
        this.macros.add(addMacros(schedule[1],schedule[2],statesChecksDevices));
        notifyDataSetChanged();
        return true;
    }
    void add(Scenarie scenarie){
        this.names.add(scenarie.name);
        this.dow.add(scenarie.dow);
        this.hoursOn.add(scenarie.hourOn);
        this.hoursOff.add(scenarie.hourOff);
        this.dateBegin.add(scenarie.dateBegin);
        this.dateEnd.add(scenarie.dateEnd);
        this.statesSelect.add(toPrimitiveBoolean(scenarie.stateSaveSelect));
        this.timers.add(scenarie.timer);
        this.macros.add(scenarie.macros);
        notifyDataSetChanged();
    }

    void remove(String name){
        int pos2 = names.indexOf(name);
        if (pos2 >= 0) {
            names.remove(pos2);
            hoursOff.remove(pos2);
            hoursOn.remove(pos2);
            dateBegin.remove(pos2);
            dateEnd.remove(pos2);
            dow.remove(pos2);
            statesSelect.remove(pos2);
            dow.trimToSize();
            statesSelect.trimToSize();
            names.trimToSize();
            hoursOff.trimToSize();
            hoursOn.trimToSize();
            dateBegin.trimToSize();
            dateEnd.trimToSize();

        timers.clear();
        macros.clear();
        timers = new ArrayList<>();
        macros = new ArrayList<>();
        for(int i = 0;i < getCount();i++){
            String [] schedule = addTimer(this.dow.get(i),this.dateBegin.get(i),this.dateEnd.get(i),this.hoursOn.get(i),this.hoursOff.get(i),i);
            this.timers.add(schedule[0]);
            this.macros.add(addMacros(schedule[1],schedule[2],toPrimitiveBooleanArray(statesSelect.get(i))));
          }
        }
        notifyDataSetChanged();
    }
    void setItem(int position,Scenarie scenarie){
        this.names.set(position,scenarie.name);
        this.hoursOn.set(position,scenarie.hourOn);
        this.hoursOff.set(position,scenarie.hourOff);
        this.dateBegin.set(position,scenarie.dateBegin);
        this.dateEnd.set(position,scenarie.dateEnd);
        this.dow.set(position,scenarie.dow);
        this.statesSelect.set(position,toPrimitiveBoolean(scenarie.stateSaveSelect));
        String [] schedule = addTimer(scenarie.dow,scenarie.dateBegin,scenarie.dateEnd,scenarie.hourOn,scenarie.hourOff,position);
        this.timers.set(position,schedule[0]);
        this.macros.set(position,addMacros(schedule[1],schedule[2],scenarie.stateSaveSelect));
        //Log.d(DomoHouseConstants.TAG,"States:")
        notifyDataSetChanged();
    }

    void clear(){
        this.names.clear();
        this.hoursOn.clear();
        this.hoursOff.clear();
        this.dateBegin.clear();
        this.dateEnd.clear();
        this.dow.clear();
        this.statesSelect.clear();
        this.timers.clear();
        this.macros.clear();
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return names.size();
    }

    @Override
    public Scenarie getItem(int position) {
        //Retorna los valores de cada Escenario
        Scenarie scenarie = new Scenarie();
        scenarie.name = this.names.get(position);
        scenarie.hourOn = this.hoursOn.get(position);
        scenarie.hourOff = this.hoursOff.get(position);
        scenarie.dateBegin = this.dateBegin.get(position);
        scenarie.dateEnd = this.dateEnd.get(position);
        scenarie.timer = this.timers.get(position);
        scenarie.macros = this.macros.get(position);
        scenarie.dow = scenarie.timer.split(" ")[1];
        scenarie.stateSaveSelect = toPrimitiveBooleanArray(this.statesSelect.get(position));
        return scenarie;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public boolean isStateCheckBox() {
        return stateCheckBox;
    }
    void removeAllSelect(){
        for(int i=0;i<listDelete.size();i++){
            remove(listDelete.get(i));
        }
        listDelete.clear();
    }
    void changeStateCheckbox(boolean checked){
        listDelete.clear();
        stateCheckBox = checked;
        notifyDataSetChanged();
    }
    ArrayList<String> getListDelete(){
        return listDelete;
    }
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        holder = null;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.scenarie_adapter, parent, false);
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvHoursOn = (TextView) convertView.findViewById(R.id.tvHourOn);
            holder.tvHoursOff = (TextView) convertView.findViewById(R.id.tvHourOff);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkSelect);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
            notifyDataSetChanged();
        }
        holder.tvName.setText(names.get(position));
        holder.tvHoursOn.setText(fromHtml("<b>Hora On:</b>"+hoursOn.get(position)+" <b>Hora Off:</b>"+hoursOff.get(position)));
        holder.tvHoursOff.setText(fromHtml("<b>Desde el</b> "+dateBegin.get(position)+" <b>Hasta el</b> "+dateEnd.get(position)));
        if(stateCheckBox){
            holder.checkBox.setVisibility(View.VISIBLE);
        }else{
            holder.checkBox.setVisibility(View.GONE);
        }
        final int pos = position;
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    listDelete.add(names.get(pos));
                    Log.d(DomoHouseConstants.TAG,"Select:["+pos+"]"+listDelete.size());
                } else{
                    int pos2 = listDelete.indexOf(names.get(pos));
                    if (pos2 >= 0) {
                        listDelete.remove(pos2);
                    }
                    Log.d(DomoHouseConstants.TAG,"Unselect:["+pos2+"] "+listDelete.size());
                }
            }
        });
        return convertView;
    }
    String fixDate(String date){
        String[] separeted;
        String dateFix;
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        separeted =  date.substring(0,5).split("/");
        dateFix = separeted[1]+"/"+separeted[0]+"/"+year;
        return dateFix;
    }
    //Crea el timer para el escenario
    String[] addTimer(String dow, String dateBegin, String dateEnd, String hourOn, String hourOff,int num){
        //EJ: timer smtwtfs 01/01-12/31  23:56  23:57  lights_on  lights_off
        String dateBeginFix;
        String dateEndFix;
        String[] separeted;
        separeted =  dateBegin.substring(0,5).split("/");
        dateBeginFix = separeted[1]+"/"+separeted[0];
        separeted =  dateEnd.substring(0,5).split("/");
        dateEndFix  =  separeted[1]+"/"+separeted[0];
        String timer = "timer "+dow+" "+dateBeginFix+"-"+dateEndFix+" ";
        String nameOn = "scen"+num+"On";
        String nameOff = "scen"+num+"Off";
        if(hourOn.contains("null")){
            timer+="00:00";
        }else{
            timer+=hourOn;
        }
        if(hourOff.contains("null")){
            timer+=" 00:00";
        }else{
            timer+=(" "+hourOff);
        }
        if(hourOn.contains("null")){
            nameOn = "null";
            timer+=" null";
        }else{
            timer+=" "+nameOn;
        }
        if(hourOff.contains("null")){
            timer+=" null";
            nameOff = "null";
        }else{
            timer+="  "+nameOff;
        }
        Log.d(DomoHouseConstants.TAG,"Timer Añadido:\n"+timer);
        return new String[]{timer,nameOn,nameOff};
    }
    //Crea las macros para cada escenario
    String[] addMacros(String nameOn,String nameOff,boolean[][] device_select){
        //   This next macro is labeled c5off.  It waits 5 minutes, then turns off d6 and d7 and turns on e2 macro c5off 5  off d6,7; on e2
        //macro c5on   0  on d7; dim d7 8
        boolean enableOn = true,enableOff=true;
        if(nameOff.contains("null")) enableOff = false;
        if(nameOn.contains("null")) enableOn = false;
       // Log.d(DomoHouseConstants.TAG,"MacronOn:"+enableOn+" MacroOff:"+enableOff);
        String macroOn="",macroOff="",states="";
        if(enableOn)macroOn = "macro "+nameOn+" 0 ";
        if(enableOff)macroOff = "macro "+nameOff+" 0 ";
        for(int i =0;i<4;i++){
            int num = nLuces;
            char house_code = 0;
            switch (i) {
                case 0:
                    num = nLuces;
                    house_code = 'A';
                    break;
                case 1:
                    num = nElectrodomesticos;
                    house_code = 'B';
                    break;
                case 2:
                    num = nVentanas;
                    house_code = 'C';
                    break;
                case 3:
                    num = nPuertas;
                    house_code = 'D';
                    break;
            }
            boolean enable = false,next = false;
            int last=0;
            //Obtiene el ultimo house_code a activar
            for (int j=0;j<num;j++){
                if(device_select[i][j]){
                    enable = true;
                    if(j > last) last = j;
                }
                for(int k = i; k < 4;k++){
                    if(k < 3) {
                        if (device_select[k + 1][j]) { //Determina si hay un house_code siguiente para añadir ';'
                            next = true;
                        }
                    }
                }
            }
            if(enable) {
                if (enableOn) macroOn += "on " + house_code;
                if (enableOff) macroOff += "off " + house_code;
            }
            states+=house_code+"(";
                for (int j = 0; j < num; j++) {
                   // states+= house_code+String.valueOf(j+1);
                    if (device_select[i][j] & enable) {
                        states += "*";
                        if(enableOn)macroOn += String.valueOf(j+1);
                        if(enableOff)macroOff += String.valueOf(j+1);
                        if (j < (num - 1) && j != last) {
                            if(enableOn)macroOn += ",";//Separa los modulos a encender por comas
                            if(enableOff)macroOff += ",";
                        }else if(next){
                            if(enableOn)macroOn += ";";
                            if(enableOff)macroOff += ";";
                        }
                    }else{
                        states+=".";
                    }
                }
            states+=");";
            }
        if(enableOn)Log.d(DomoHouseConstants.TAG,"MacroOn:"+macroOn+"\n");
        if(enableOff)Log.d(DomoHouseConstants.TAG,"MacroOff:"+macroOff+"\n");
        Log.d(DomoHouseConstants.TAG,"States:"+states);
       return new String[]{(enableOn) ? macroOn : null,(enableOff) ? macroOff : null,states};
    }
    //Retorna la lista con el nombre y los timers de cada escenario
    String getTimers(){
        String result="";
        for (int i = 0 ; i < getCount(); i++){
            result +=("#"+i+"->"+names.get(i)+"@states->"+macros.get(i)[2]+"\r\n");
            result +=(timers.get(i)+"\r\n");
        }
        return  result;
    }
    //Retorna los macros, los cuales son los respectivos comandos heyu
    String getMacros(){
        String result = "";
        for (int i = 0 ; i < getCount(); i++){
            if(macros.get(i)[0] != null){
                result +=(macros.get(i)[0]+"\r\n");
            }
            if(macros.get(i)[1] != null){
                result +=(macros.get(i)[1]+"\r\n");
            }
        }
        return result;
    }
}
