/*
 * Michael Vargas Copyright (c) 2017. - Colombia
 */

package msvargas97.android.domohouse;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ControlSwitchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ControlSwitchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ControlSwitchFragment extends Fragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private int mParam2;
    private int position;
    final boolean[] typeBtn = new boolean[]{DomoHouseConstants.ON_OFF,DomoHouseConstants.ON_OFF, DomoHouseConstants.CLOSE_OPEN, DomoHouseConstants.CLOSE_OPEN, };
    ArrayList<String>[] names = new ArrayList[4];
    ArrayList<Integer>[] states = new ArrayList[4];
   // ArrayList<String> currentName = new ArrayList<String>();
    //ArrayList<Integer> currentStates = new ArrayList<Integer>();
    DomoHouseAdapter domoHouseAdapter;
    private OnFragmentInteractionListener mListener;
    ImageView imgTitle;
    ListView list;
    public ControlSwitchFragment() {
        // Required empty public constructor
    }

    public ArrayList<Integer>[] getStates() {
        return states;
    }

    public void setStates(ArrayList<Integer>[] states) {
        this.states = states;
        updateListView();
    }
    void updateListView(){
        if(domoHouseAdapter != null){//Actualiza la lista
            domoHouseAdapter.setStates(this.states[position]);
            list.setAdapter(domoHouseAdapter);
        }
    }
    public ArrayList[] getNames() {
        return names;
    }

    public void setNames(ArrayList[] names) {
        this.names = names;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ControlSwitchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ControlSwitchFragment newInstance(String param1, int param2) {
        ControlSwitchFragment fragment = new ControlSwitchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_control_switch, container, false);
        if (mListener != null) {
            mListener.onFragmentInteraction(mParam1,mParam2);
        }
        imgTitle = (ImageView) view.findViewById(R.id.ImgTitle);

        list = (ListView) view.findViewById(R.id.listViewControl);


        if(position == DomoHouseConstants.PUERTAS){
            imgTitle.setImageResource(R.drawable.puertas_tittle);
           // btnOff.setImageResource(R.drawable.closetodo);
           // btnOn.setImageResource(R.drawable.opentodo);
        }else  if(position == DomoHouseConstants.LUCES){
            imgTitle.setImageResource(R.drawable.luces_title);
        }else  if(position == DomoHouseConstants.VENTANAS){
           //btnOff.setImageResource(R.drawable.closetodo);
           //btnOn.setImageResource(R.drawable.opentodo);
            imgTitle.setImageResource(R.drawable.ventanas_title);
        }else  if(position == DomoHouseConstants.ElECTRO){
            imgTitle.setImageResource(R.drawable.electro_tittle);
        }

        domoHouseAdapter = new DomoHouseAdapter(getActivity().getApplicationContext(),typeBtn[position],names[position],states[position]);
        list.setAdapter(domoHouseAdapter);
        final int pos = position;
        domoHouseAdapter.setmListener(new DomoHouseAdapter.AdapterInterface() {
            @Override
            public void OnSwitchListener(int position, Integer state) {
                Log.d(DomoHouseConstants.TAG,"Presiono #Control:"+pos+" State["+position+"]="+state);
                if(mListener != null){
                    char HU = (char) (pos + 'A');
                    String house_code = HU+""+(position+1);
                    mListener.OnSwitchClickListener(house_code,state);
                }
            }
        });

        return view;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String title,int num_fragment);
        void OnSwitchClickListener(String name,Integer state);
    }
}
