package msvargas97.android.domohouse;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements LoginFragment.OnFragmentInteractionListener
        ,RegisterFragment.OnFragmentInteractionListener,ControlFragment.OnFragmentInteractionListener
        ,ScenariesFragment.OnFragmentInteractionListener,SettingsFragment.OnFragmentInteractionListener
        ,ControlSwitchFragment.OnFragmentInteractionListener, FullScreenDialog.OnFragmentInteractionListener,View.OnClickListener {
    //TODO: Creación de variables
    String urlServer = "192.168.1.29"; //IP publica o local del servidor web
    String port = "8080";                // Puerto en el cual esta configurado el servidor
    String username,password,house_code; //Variables para almacenar los datos del usuario
    int num=10;                          //Numero maximo de modulos a conectar
    int nLuces,nElectrodomesticos,nPuertas,nVentanas;//El numero de elementos en cada seccion
    boolean login=false,saveUser,wait;   //Verifican el estado de partes de la aplicación
    boolean exit = false;                //Verifica si se desea salir de la aplciación
    int select = 0,posPort;              //El numero de fragmento anterior y el numero de puerto
    JSONObject jsonConfig;               //Objeto JSON para almacenar la configuración enviada por el servidor
    SharedPreferences sharedPreferences; //Guarda parametros de la aplicación como el usuario y contraseña
    SharedPreferences.Editor editor;     //Intermediario para leer o escribir parametros del sharedPreferences
    ArrayList<String> An,Bn,Cn,Dn;       //Lista dinamica para almacenar los nombres en cada sección
    ArrayList<Integer> stateAn,stateBn,stateCn,stateDn;//Lista dinamica para el estado de los modulos
    //TODO: Creacion de fragmentos
    LoginFragment loginFragment = null;  //Crea el fragmento para el login
    ControlFragment controlFragment= null;//Crea el fragmento para seleccionar una seccion a controlar
    ScenariesFragment scenariesFragment= null; //Sección para configurar los escenarios del modulo CM11
    RegisterFragment registerFragment= null;    //Fragmento que contiene webview para cargar paginas del servidor
    SettingsFragment settingsFragment= null;    //Fragmento para ajustar parametros de la aplicación
    ControlSwitchFragment controlSwitchFragment = null;//Fragmento para controlar una sección seleccionada
    DomoHouseFragmentManager domoHouseFragmentManager = null;//Manejador de fragmentos
    FullScreenDialog fullScreenDialog = null;   //Crea un fragmento para crear o modificar los escenarios
    BottomNavigationView navigation = null;     //Crea la barra inferior de la aplicación
    ImageButton btnOn,btnOff;                   //Botones para encender  o apagar todos los modulos deu na sección
    LinearLayout linearBtns;                    //Contenedor de los botones, para hacerlos visibles o ono


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linearBtns = (LinearLayout) findViewById(R.id.layoutBtns);
        btnOff = (ImageButton) findViewById(R.id.btnOff);
        btnOn = (ImageButton) findViewById(R.id.btnOn);
        btnOff.setOnClickListener(this);
        btnOn.setOnClickListener(this);

        //TODO:Carga fragmentos y crea el vector fragments para facilitar la transicion de fragmentos, usando el manejador
        loginFragment = LoginFragment.newInstance(getString(R.string.sign_in),DomoHouseConstants.LOGIN_FRAGMENT);
        scenariesFragment = ScenariesFragment.newInstance(getString(R.string.scenarios),DomoHouseConstants.SCENARIES_FRAGMENT);
        registerFragment = RegisterFragment.newInstance(getBaseUrl("register"),getString(R.string.register),DomoHouseConstants.REGISTER_FRAGMENT);
        settingsFragment = SettingsFragment.newInstance(getString(R.string.action_settings),DomoHouseConstants.SETTINGS_FRAGMENT);
        controlFragment = ControlFragment.newInstance(getString(R.string.house_control),DomoHouseConstants.CONTROL_FRAGMENT);
        controlSwitchFragment = ControlSwitchFragment.newInstance(getString(R.string.house_control),DomoHouseConstants.CONTROL_SW_FRAGMENT);
        fullScreenDialog = new FullScreenDialog();
        //TODO:Crea el manejador de fragmentos
        domoHouseFragmentManager = new DomoHouseFragmentManager(getSupportFragmentManager(),
                new Fragment[]{loginFragment,controlFragment,scenariesFragment,registerFragment,settingsFragment,
                        controlSwitchFragment,fullScreenDialog},
                R.id.content);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        sharedPreferences = getSharedPreferences(DomoHouseConstants.PREFS_NAME, 0);
        urlServer = sharedPreferences.getString(DomoHouseConstants.IP_SERVER,urlServer);
        port = sharedPreferences.getString(DomoHouseConstants.PORT_SERVER,port);
        posPort = sharedPreferences.getInt(DomoHouseConstants.POS_PORT_SERVER,posPort);
        saveUser = sharedPreferences.getBoolean(DomoHouseConstants.REMEMBER_USER, false);
        String strConfig = sharedPreferences.getString(DomoHouseConstants.CONFIG_JSON,null);

        if(strConfig != null){
            try {
                jsonConfig = new JSONObject(strConfig); //Restaura configuracion de la lista botones para el DomoHouse Adapter
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        domoHouseFragmentManager.setFragment(DomoHouseConstants.LOGIN_FRAGMENT);
        navigation.getMenu().getItem(2).setEnabled(false);
        if (saveUser) {
            username = sharedPreferences.getString(DomoHouseConstants.LAST_USER,null);
            password = sharedPreferences.getString(DomoHouseConstants.LAST_PASSWORD,null);
            loginFragment.setEdUser(username);
            OnListenerButtonLogin(username,password,true);
        }
    }
    //TODO:Interface comunicacion entre el metodo HttpPostResquest y el MainThread
    @Override
    protected void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        String actionName = extras.getString(DomoHouseConstants.SERVER_REQUEST,null);
        String result = extras.getString(DomoHouseConstants.SERVER_RESULT,"Falló la conexión con el servidor");
        Log.e(DomoHouseConstants.TAG,"ActionName:"+actionName+"\nResultado:"+result);
        if(result != null) {
            //showToast(result,Toast.LENGTH_LONG);
            switch(actionName) {
                case "login": //Respuesta y Verificacion de login
                    if (result.contains("Bienvenido!")) {
                        getConfigDomoHouse();
                        showToast(result,Toast.LENGTH_LONG);
                        loginFragment.inputLogin(false);
                        loginFragment.setTextButtonLogin(getString(R.string.close_sign_in));
                        login = true;
                        editor = sharedPreferences.edit();
                        editor.putBoolean(DomoHouseConstants.REMEMBER_USER, saveUser);
                        editor.putString(DomoHouseConstants.LAST_USER, username);
                        editor.putString(DomoHouseConstants.LAST_PASSWORD, password);
                        editor.apply();
                        if (saveUser) {
                            loginFragment.setEdUser(username);
                        }
                    } else {
                        if (result.contains("Contraseña")) {
                            loginFragment.showErrorShakePass();
                        } else if (result.contains("Usuario")) {
                            loginFragment.showErrorShakeUser();
                        } else if (result.contains("Todos")) {
                            loginFragment.showErrorShakePass();
                            loginFragment.showErrorShakeUser();
                        }
                        showToast(result,Toast.LENGTH_LONG);
                        username = password = null;
                        login = false;
                    }
                    break;
                case "config":
                    try {
                        jsonConfig = new JSONObject(result); //Obtiene el Json enviado por el servidor
                        //Lee la configuración del grupo A - luces número de luces y nombres
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //showToast(result,Toast.LENGTH_LONG);
                        String strConfig = sharedPreferences.getString(DomoHouseConstants.CONFIG_JSON,null);
                        if(strConfig != null){
                            try {
                                jsonConfig = new JSONObject(strConfig);
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            showToast("Error al leer configuración del Servidor\nSe estableció la ultima configuración",Toast.LENGTH_LONG);
                        }
                    }
                    try {
                    nLuces = Integer.parseInt(jsonConfig.getString("nLuces"));
                    An = new ArrayList<>();
                    stateAn = new ArrayList<>();
                    //Log.d(TAG,"Luces:\n");
                    for (int i = 0; i < nLuces; i++) {
                        String house_code = "A" + (i + 1);
                        An.add(jsonConfig.getString(house_code));
                        stateAn.add(jsonConfig.getInt("state:"+house_code));
                        //Log.d(TAG,An.get(i)+"\n");
                    }
                    //Log.d(TAG,"Electrodomesticos:\n");
                    //Lee configuración de grupo B - Electrodomesticos
                    nElectrodomesticos = Integer.parseInt(jsonConfig.getString("nElectrodomesticos"));
                    Bn = new ArrayList<>();
                    stateBn = new ArrayList<>();
                    for (int i = 0; i < nElectrodomesticos; i++) {
                        String house_code = "B" + (i + 1);
                        Bn.add(jsonConfig.getString(house_code));
                        stateBn.add(jsonConfig.getInt("state:"+house_code));
                        //  Log.d(TAG,Bn.get(i)+"\n");
                    }
                    //Log.d(TAG,"Ventanas:\n");
                    //Lee el grupo C - Ventanas
                    nVentanas = Integer.parseInt(jsonConfig.getString("nVentanas"));
                    Cn = new ArrayList<>();
                    stateCn = new ArrayList<>();
                    for (int i = 0; i < nVentanas; i++) {
                        String house_code = "C" + (i + 1);
                        Cn.add(jsonConfig.getString(house_code));
                        stateCn.add(jsonConfig.getInt("state:"+house_code));
                        // Log.d(TAG,Cn.get(i)+"\n");
                    }
                    //Lee configuracion del grupo D - Puertas;
                    nPuertas = Integer.parseInt(jsonConfig.getString("nPuertas"));
                    Dn = new ArrayList<>();
                    stateDn = new ArrayList<>();
                        Log.d(DomoHouseConstants.TAG,"Puertas:"+nPuertas);
                    for (int i = 0; i < nPuertas; i++) {
                        String house_code = "D" + (i + 1);
                        Dn.add(jsonConfig.getString(house_code));
                        stateDn.add(jsonConfig.getInt("state:"+house_code));
                        Log.d(DomoHouseConstants.TAG,"Puertas:"+jsonConfig.getString(house_code));
                    }

                    editor = sharedPreferences.edit();
                    editor.putString(DomoHouseConstants.CONFIG_JSON, result);//Guarda la configuración
                    editor.apply();
                    //setragmentNavigation(DomoHouseConstants.CONTROL_FRAGMENT);
                    controlSwitchFragment.setNames(new ArrayList[]{An,Bn,Cn,Dn});
                    controlSwitchFragment.setStates(new ArrayList[]{stateAn,stateBn,stateCn,stateDn});
                    domoHouseFragmentManager.setFragment(DomoHouseConstants.CONTROL_FRAGMENT);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case "control":
                    showToast(result, Toast.LENGTH_SHORT);
                    break;
                case "current_state":
                    try {
                        JSONObject jsonStates = new JSONObject(result);
                        Log.d(DomoHouseConstants.TAG,"HC:"+house_code);
                        String house_code = jsonStates.getString("house_code");
                        for (int i = 1; i <= num; i++) {
                            int value = jsonStates.getInt("state:"+house_code+i);
                            switch (house_code){
                                case "A":
                                    stateAn.set(i-1,value);
                                    break;
                                case "B":
                                    stateBn.set(i-1,value);
                                    break;
                                case "C":
                                    stateCn.set(i-1,value);
                                    break;
                                case "D":
                                    stateDn.set(i-1,value);
                                    break;
                            }
                           // Log.d(DomoHouseConstants.TAG,house_code+i+":"+jsonStates.getInt("state:"+house_code+i));
                        }
                        controlSwitchFragment.setStates(new ArrayList[]{stateAn,stateBn,stateCn,stateDn});
                       // domoHouseFragmentManager.setFragment(DomoHouseConstants.CONTROL_SW_FRAGMENT);
                      //  wait = false;
                    }catch (JSONException e) {
                        e.printStackTrace();
                        showToast(result, Toast.LENGTH_SHORT);
                    }
                    break;
                case "schedule_write":
                    if(!result.contains("Fallo")){
                        String [] separed = result.split("\n");
                        if(separed.length > 2) {
                            showToast("OK\n" + separed[3], Toast.LENGTH_LONG);
                        }
                    }else {
                        showToast(result, Toast.LENGTH_SHORT);
                    }
                    break;
                case "schedule_read":
                    if(result.contains("#")) {
                        scenariesFragment.scenarieAdapter.clear();
                        String[] separed = result.split("\n");
                        int num_scenaries = Integer.parseInt(separed[3].split("->")[1]);
                        //ArrayList<ScenarieAdapter.Scenarie[]> timers = new ArrayList<>();
                        ScenarieAdapter.Scenarie scenaries[] = new ScenarieAdapter.Scenarie[num_scenaries];
                        for (int i = 0; i < num_scenaries; i++) {
                            scenaries[i] = new ScenarieAdapter.Scenarie();
                        }
                        Log.d(DomoHouseConstants.TAG, "NumEscenarios:" + num_scenaries);
                        int pos = 0;
                        int start_line_timer = 7;
                        int line_timers = (num_scenaries * 2) + start_line_timer;
                        int line_macros = line_timers + 1;
                        for (int i = start_line_timer; i < line_timers; i++) {
                            if (i % 2 == 0) {//Si la linea es par, es el contenido del timer
                                scenaries[pos].timer = separed[i];
                                String[] timer = scenaries[pos].timer.split(" ");
                                scenaries[pos].dow = timer[1];
                                String[] date = timer[2].split("-");
                                scenaries[pos].dateBegin = scenariesFragment.scenarieAdapter.fixDate(date[0]);
                                scenaries[pos].dateEnd = scenariesFragment.scenarieAdapter.fixDate(date[1]);
                                if(!timer[5].contains("null")){
                                    scenaries[pos].hourOn = timer[3];
                                }else{
                                    scenaries[pos].hourOn = timer[5];
                                }
                                if(!timer[6].contains("null")){
                                    scenaries[pos].hourOff = timer[4];
                                }else{
                                    scenaries[pos].hourOff = timer[6];
                                }
                                if (!scenaries[pos].hourOff.contains("null") && !scenaries[pos].hourOn.contains("null")) {
                                    scenaries[pos].macros[0] = separed[line_macros];
                                    scenaries[pos].macros[1] = separed[line_macros + 1];
                                    line_macros += 2;
                                } else{
                                    if(!scenaries[pos].hourOn.contains("null"))scenaries[pos].macros[0] = separed[line_macros];
                                    else scenaries[pos].macros[0] = null;
                                    if(!scenaries[pos].hourOff.contains("null"))scenaries[pos].macros[1] =  separed[line_macros];
                                    else scenaries[pos].macros[1] = null;
                                    line_macros++;
                                }
                                scenariesFragment.scenarieAdapter.add(scenaries[pos]);
                                //Log.d(DomoHouseConstants.TAG,"Timer:"+separed[i]+" Dow:"+scenaries[pos].dow+
                                //       "\ndateBegin:"+scenaries[pos].dateBegin+" dateEnd:"+scenaries[pos].dateEnd+
                                //       "\nhourOn:"+scenaries[pos].hourOn+" hourOff:"+scenaries[pos].hourOff+
                                //       "\nMacroOn:"+scenaries[pos].macros[0]+"MacroOff:"+scenaries[pos].macros[1]+" states:"+scenaries[pos].macros[2]);
                                pos++;
                            } else {
                                String[] info_line = separed[i].split("@");
                                scenaries[pos].name = info_line[0].substring(4);
                                scenaries[pos].macros[2] = info_line[1].split("->")[1];
                                String[] states = scenaries[pos].macros[2].split(";");
                                // Log.d(DomoHouseConstants.TAG,"name:"+scenaries[pos].name+"\nStates:"+states[0]);
                                for (int k = 0; k < 4; k++) {
                                    int num = 0;
                                    if (k == 0) num = nLuces;
                                    else if (k == 1) num = nElectrodomesticos;
                                    else if (k == 2) num = nVentanas;
                                    else if (k == 3) num = nPuertas;

                                    for (int j = 2; j < (num + 2); j++) {
                                        if(j < states[k].length()) {
                                            if (states[k].charAt(j) == '*')
                                                scenaries[pos].stateSaveSelect[k][j - 2] = true;
                                            else scenaries[pos].stateSaveSelect[k][j - 2] = false;
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        showToast(result,Toast.LENGTH_LONG);
                    }
                    break;
            }
        }
        super.onNewIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings){
            domoHouseFragmentManager.setFragment(DomoHouseConstants.SETTINGS_FRAGMENT);
            return true;
        }else if( id == R.id.action_credits){
            LayoutInflater inflater2 = getLayoutInflater();
            final View creditosView = inflater2.inflate(R.layout.creditos_udi,null);
            builder.setTitle("Creditos:").setCancelable(true);
            builder.setView(creditosView)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        }else if(id == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        Log.d(DomoHouseConstants.TAG, "Select:"+select+" Frg:"+getSupportFragmentManager().getBackStackEntryCount());
        select = getSupportFragmentManager().getBackStackEntryCount();
        if(select > 1){
            exit = false;
            super.onBackPressed();
        }else{
            if(exit)
                finish();
            else{
                showToast("Presione una vez más para salir de la aplicación",Toast.LENGTH_LONG);
                exit = true;
            }
        }
        //super.onBackPressed();
    }

    //TODO: FUNCIONES INTERFACES ONLISTENER
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_login:
                    domoHouseFragmentManager.setFragment(DomoHouseConstants.LOGIN_FRAGMENT);
                    return true;
                case R.id.navigation_house_control:
                    domoHouseFragmentManager.setFragment(DomoHouseConstants.CONTROL_FRAGMENT);
                    return true;
                case R.id.navigation_scenaries:
                    domoHouseFragmentManager.setFragment(DomoHouseConstants.SCENARIES_FRAGMENT);
                    return true;
            }
            return false;
        }
    };
    //TODO: FUNCIONES MAIN
    private void getConfigDomoHouse() {
        String actionName = "config";
        String baseUrl = getBaseUrl(actionName);
        new HttpPostRequest(this,false,null).execute(baseUrl, null, actionName);
    }
    String getBaseUrl(String actionName){
        String url;
        if(port.contains("default")){
            url = "http://" + urlServer + "/" + actionName + ".php";
        }else{
            url = "http://" + urlServer + ":" + port + "/" + actionName + ".php";
        }
        return url;
    }
    void setFragmentNavigation(int numFragment){
        MenuItem item = navigation.getMenu().getItem(numFragment).setChecked(true);
        navigation.setSelectedItemId(item.getItemId());
    }
    public void showToast(String msg,int duration){
        Toast.makeText(getApplicationContext(),msg,duration).show();
    }
    public String toJSON(String[] name,String[] values){
        JSONObject jsonObject= new JSONObject();
        try {
            for (int i=0;i<name.length;i++)
                jsonObject.put(name[i],values[i]);
            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }
    void updateListEscenarie(int size){
        if(login && size == 0){
            String url_x10sched;
            if(port.contains("default")) {
                url_x10sched = "http://" + urlServer+"/includes/x10.sched";
            }else{
                url_x10sched = "http://" + urlServer + ":" + port + "/includes/x10.sched";
            }
            new HttpPostRequest(this,true,"Actualizando escenarios...").execute(url_x10sched, null, "schedule_read");
        }
    }
    String getDate(String pattern){
        Date today;
        SimpleDateFormat formatter;
        Locale currentLocale = getResources().getConfiguration().locale;
        formatter = new SimpleDateFormat(pattern, currentLocale);
        today = new Date();
        return formatter.format(today);
    }
    void sendAndSaveScenaries(){
        String actionName = "schedule";
        String baseUrl = getBaseUrl(actionName);
        String scheduleFile = "#Archivo Auto-generado por DomoHouse App Android V1.0 \r\n";
        scheduleFile += "#A L G O R I T M O - H E Y U - S C H E D U L E - J A V A\r\n#Autor->Michael Vargas - Copyright 2017\r\n";
        scheduleFile += "#Numero de Escenarios creados->" + scenariesFragment.scenarieAdapter.getCount() + "\r\n";
        scheduleFile += "#Info->" + getDate("EEE, d MMM yyyy HH:mm") + "\r\n#Usuario->" + username + "\r\n";
        scheduleFile += "################### T I M E R S ###################\r\n";
        scheduleFile += scenariesFragment.scenarieAdapter.getTimers();
        scheduleFile += "################### M A C R O S ###################\r\n";
        scheduleFile += scenariesFragment.scenarieAdapter.getMacros();
        scheduleFile += "###################################################\r\n";
        //Log.d(DomoHouseConstants.TAG,scheduleFile);
        new HttpPostRequest(this, true, "Enviando datos al servidor\nPor favor espere...").execute(baseUrl, scheduleFile, actionName + "_write");
    }
    int checkedNameScenarie(String name){
        return scenariesFragment.scenarieAdapter.indexOf(name);
    }
    //TODO: INTERFACES DE FRAGMENTOS
    @SuppressWarnings("ConstantConditions")
    @Override
    public void onFragmentInteraction(String title, int num_fragment) {
        switch (num_fragment){
            case DomoHouseConstants.CONTROL_SW_FRAGMENT:
                linearBtns.setVisibility(View.VISIBLE);
                break;
            case DomoHouseConstants.SETTINGS_FRAGMENT:
            case DomoHouseConstants.REGISTER_FRAGMENT:
                navigation.setVisibility(View.GONE);
                linearBtns.setVisibility(View.GONE);
            default:
                navigation.setVisibility(View.VISIBLE);
                linearBtns.setVisibility(View.GONE);
                break;
        }

        if ((num_fragment == DomoHouseConstants.SETTINGS_FRAGMENT) || (num_fragment == DomoHouseConstants.FULL_SCREEN_FRAGMENT) ||
                (num_fragment == DomoHouseConstants.REGISTER_FRAGMENT) || (num_fragment == DomoHouseConstants.CONTROL_SW_FRAGMENT)){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            if(num_fragment == DomoHouseConstants.FULL_SCREEN_FRAGMENT)
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
            else
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);
        }else{
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }
        if(num_fragment == DomoHouseConstants.SETTINGS_FRAGMENT){
        settingsFragment.setValues(urlServer,posPort);
        }
        //Log.d(DomoHouseConstants.TAG,"Current:"+domoHouseFragmentManager.getCurrentFragment()+" Last:"+domoHouseFragmentManager.getLastFragment());
        getSupportActionBar().setTitle(title);
        domoHouseFragmentManager.setTransitionFragment(num_fragment);
    }

    @Override
    public void onChangeSettings(String urlServer, String port,int posPort) {
        this.urlServer = urlServer;
        this.port = port;
        this.posPort = posPort;
        editor = sharedPreferences.edit();
        editor.putString(DomoHouseConstants.IP_SERVER,urlServer);
        editor.putString(DomoHouseConstants.PORT_SERVER,port);
        editor.putInt(DomoHouseConstants.POS_PORT_SERVER,posPort);
        editor.apply();
        showToast("Datos asignados correctamente",Toast.LENGTH_LONG);
    }

    @Override
    public void onSaveButton(String name, String dow, String dateBegin, String dateEnd, String hourOn, String hourOff, boolean[][] devices_select) {
        if(fullScreenDialog.isNew_scenarie()) {
            scenariesFragment.scenarieAdapter.setNumElements(nLuces, nPuertas, nVentanas, nPuertas);
            scenariesFragment.scenarieAdapter.add(name, dow, hourOn, hourOff, dateBegin, dateEnd, devices_select);
            showToast("Para actualizar la información con el servidor presione el boton de guardar", Toast.LENGTH_LONG);
        }else{
            ScenarieAdapter.Scenarie scenarie = new ScenarieAdapter.Scenarie();
            scenarie.name = name;
            scenarie.dow = dow;
            scenarie.dateBegin = dateBegin;
            scenarie.dateEnd = dateEnd;
            scenarie.hourOn = hourOn;
            scenarie.hourOff = hourOff;
            scenarie.stateSaveSelect = devices_select;
            scenariesFragment.scenarieAdapter.setNumElements(nLuces, nPuertas, nVentanas, nPuertas);
            //scenarie.macros = scenariesFragment.scenarieAdapter.addMacros()
            showToast("Se actualizó\r\nEscenario No "+String.valueOf(scenariesFragment.getWhich()+1),Toast.LENGTH_LONG);
            scenariesFragment.scenarieAdapter.setItem(scenariesFragment.getWhich(),scenarie);
        }
        domoHouseFragmentManager.setFragment(DomoHouseConstants.SCENARIES_FRAGMENT);
    }

    @Override
    public void OnFabBtnListener(int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog;
        //Log.d(DomoHouseConstants.TAG,"Fab Btn Presiono:"+id);
        if(!login) return;
        switch (id){
            case R.id.fabAdd:
                String nameScenarie = "Escenario No "+(scenariesFragment.scenarieAdapter.getCount()+1);
                fullScreenDialog.setNew_scenarie(true);
                fullScreenDialog.setDevice_name(new ArrayList[]{An,Bn,Cn,Dn});
                fullScreenDialog.setTitleFragment(nameScenarie,DomoHouseConstants.FULL_SCREEN_FRAGMENT);
                fullScreenDialog.clearList();
                domoHouseFragmentManager.setFragment(DomoHouseConstants.FULL_SCREEN_FRAGMENT);
                break;
            case R.id.fabDelete:
                builder.setTitle("Eliminar Escenarios")
                        .setMessage("Esta seguro que desea eliminar los escenarios seleccionados?")
                        .setPositiveButton("ELIMINAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                scenariesFragment.removeListScenarie();
                            }
                        })
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                dialog = builder.create();
                dialog.show();
                break;
            case R.id.fabSave:
                if(scenariesFragment.scenarieAdapter.getCount() > 0) {
                    builder.setTitle("Actualizar Escenarios")
                            .setMessage("A continuación se enviara la configuración de escenarios al servidor")
                            .setPositiveButton("ENVIAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    sendAndSaveScenaries();
                                }
                            })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    dialog = builder.create();
                    dialog.show();
                }else{
                    showToast("No hay escenarios configurados",Toast.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void OpenFullScreenDialog(ScenarieAdapter.Scenarie scenarieLoad,int which) {
        fullScreenDialog.setScenarieLoad(scenarieLoad);
        fullScreenDialog.clearList();
        fullScreenDialog.setTitleFragment("Escenario No "+(which+1),DomoHouseConstants.FULL_SCREEN_FRAGMENT);
        fullScreenDialog.setNew_scenarie(false);
        fullScreenDialog.setDevice_name(new ArrayList[]{An,Bn,Cn,Dn});
        domoHouseFragmentManager.setFragment(DomoHouseConstants.FULL_SCREEN_FRAGMENT);
    }

    @Override
    public void updateScenaries(int size) {
        updateListEscenarie(size);
    }

    @Override
    public void OnSwitchClickListener(String name, Integer state) {
        if(login) {
            String actionName = "control";
            String baseUrl = getBaseUrl(actionName);
            String strJson = toJSON(new String[]{"name","state","all"}, new String[]{name, state.toString(),"0"});
          //  Log.d(DomoHouseConstants.TAG,"Json:"+strJson);
            new HttpPostRequest(this, false, null).execute(baseUrl, strJson, actionName);
        }
    }

    @Override
    public void onClickListenerButtons(int idBtn) {
        if (login) {
            if(!wait) {
                controlSwitchFragment.setNames(new ArrayList[]{An, Bn, Cn, Dn});
                controlSwitchFragment.setStates(new ArrayList[]{stateAn, stateBn, stateCn, stateDn});
                switch (idBtn) {
                    case R.id.btnElectro:
                        controlSwitchFragment.setPosition(DomoHouseConstants.ElECTRO);
                        btnOn.setImageResource(R.drawable.entodo);
                        btnOff.setImageResource(R.drawable.aptodo);
                        house_code = "B";
                        num = nElectrodomesticos;
                        break;
                    case R.id.btnVentanas:
                        controlSwitchFragment.setPosition(DomoHouseConstants.VENTANAS);
                        btnOff.setImageResource(R.drawable.closetodo);
                        btnOn.setImageResource(R.drawable.opentodo);
                        house_code = "C";
                        num = nVentanas;
                        break;
                    case R.id.btnLuces:
                        controlSwitchFragment.setPosition(DomoHouseConstants.LUCES);
                        btnOn.setImageResource(R.drawable.entodo);
                        btnOff.setImageResource(R.drawable.aptodo);
                        house_code = "A";
                        num = nLuces;
                        break;
                    case R.id.btnPuertas:
                        controlSwitchFragment.setPosition(DomoHouseConstants.PUERTAS);
                        btnOff.setImageResource(R.drawable.closetodo);
                        btnOn.setImageResource(R.drawable.opentodo);
                        house_code = "D";
                        num = nPuertas;
                        break;
                }
                String baseUrl = getBaseUrl("config");
                String strJson = toJSON(new String[]{"house_code", "num"}, new String[]{house_code, String.valueOf(num)});
                new HttpPostRequest(this, false, null).execute(baseUrl, strJson, "current_state");
                domoHouseFragmentManager.setFragment(DomoHouseConstants.CONTROL_SW_FRAGMENT);
               // wait = true;
            }else{
                showToast("Esperando respuesta del servidor...",Toast.LENGTH_SHORT);
            }
        }else{
            showToast("Por favor inicie sesión",Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void OnListenerButtonLogin(String user, String password, boolean saveUser) {
        if(!login) {
            String actionName = "login";
            String baseUrl = getBaseUrl(actionName);
            String strJson = toJSON(new String[]{"username","password"}, new String[]{user, password});
            new HttpPostRequest(this,true,"Iniciando Sesión...").execute(baseUrl, strJson, actionName);
            this.username = user;
            this.password = password;
            this.saveUser = saveUser;
            navigation.getMenu().getItem(2).setEnabled(true);
        }else{
            loginFragment.setTextButtonLogin("ENTRAR");
            loginFragment.inputLogin(true);
            loginFragment.resetInput();
            this.username = null;
            this.password = null;
            login = false;
            scenariesFragment.scenarieAdapter.clear();
            navigation.getMenu().getItem(2).setEnabled(false);
        }
    }

    @Override
    public void OnListenerCheckSaveUser(boolean isChecked) {
        saveUser = isChecked;
        editor = sharedPreferences.edit();
        editor.putBoolean(DomoHouseConstants.REMEMBER_USER,saveUser);
        editor.apply();
       // editor.putString(DomoHouseConstants.IP_SERVER,urlServer);
        //editor.putString(DomoHouseConstants.PORT_SERVER,port);
    }
    @Override
    public void OnListenerRegister(int action) {
        if(action == 0){
            registerFragment.changeParams(getBaseUrl("register"),getString(R.string.register),DomoHouseConstants.REGISTER_FRAGMENT);
        }else  if(action == 1){
            registerFragment.changeParams(getBaseUrl("forget"),getString(R.string.olvidaste_tu_cuenta),DomoHouseConstants.REGISTER_FRAGMENT);
        }else  if(action == 2){
            registerFragment.changeParams(getBaseUrl("change_password"),getString(R.string.cambiar_contrase_a),DomoHouseConstants.REGISTER_FRAGMENT);
        }
        //Carga el webview para el registro o olvidar contraseña
        domoHouseFragmentManager.setFragment(DomoHouseConstants.REGISTER_FRAGMENT);
    }

    @Override
    public void onClick(View v) {

        Integer value = 0;
        if (v.getId() == R.id.btnOff){
            value = 0;
            Log.d(DomoHouseConstants.TAG,"APAGANDO TODO");
        }else if (v.getId() == R.id.btnOn){
            value = 1;
            Log.d(DomoHouseConstants.TAG,"ENCENDIENDO TODO");
        }
        controlSwitchFragment.domoHouseAdapter.setStateAll(value);
        switch (controlSwitchFragment.getPosition()) {
            case DomoHouseConstants.ElECTRO:
                house_code = "B";
                break;
            case DomoHouseConstants.LUCES:
                house_code = "A";
                break;
            case DomoHouseConstants.PUERTAS:
                house_code = "D";
                break;
            case DomoHouseConstants.VENTANAS:
                house_code = "C";
                break;
        }
        String actionName = "control";
        String baseUrl = getBaseUrl(actionName);
        String strJson = toJSON(new String[]{"name","state","all"}, new String[]{house_code,value.toString(),"1"});
        // Log.d(DomoHouseConstants.TAG,"Json:"+strJson);
        new HttpPostRequest(this, false, null).execute(baseUrl, strJson, actionName);
    }


}

