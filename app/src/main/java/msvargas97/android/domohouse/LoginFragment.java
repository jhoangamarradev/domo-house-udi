package msvargas97.android.domohouse;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private int mParam2;

    private OnFragmentInteractionListener mListener;
    Button btnLogin;
    EditText edPass,edUser;
    CheckBox checkSaveUser;
    TextView txtForgetPass,txtRegister,txtChangePsswd;
    String textUser = null;
    String textButton = "ENTRAR";
    Boolean stateViews = true;
    TextInputLayout hideEdit;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, int param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2,param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("textButton",textButton);
        outState.putBoolean("stateViews",stateViews);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        btnLogin = (Button) view.findViewById(R.id.btnSing);
        edPass = (EditText) view.findViewById(R.id.etPassword);
        edUser = (EditText) view.findViewById(R.id.edUser);
        checkSaveUser = (CheckBox) view.findViewById(R.id.checkSaveUser);
        txtForgetPass = (TextView)view.findViewById(R.id.txtForgetPAss);
        hideEdit = (TextInputLayout) view.findViewById(R.id.edPass);
        txtRegister = (TextView)  view.findViewById(R.id.tvRegister);
        txtChangePsswd = (TextView) view.findViewById(R.id.txtChangePass);
        if (mListener != null) {
            mListener.onFragmentInteraction(mParam1,mParam2);
        }
        if(textUser != null){
            checkSaveUser.setChecked(true);
            edUser.setText(textUser);
        }
        else{
            checkSaveUser.setChecked(false);
        }
        if(savedInstanceState != null){
            stateViews = savedInstanceState.getBoolean("stateViews");
            textButton = savedInstanceState.getString("textButton");
        }
        //Log.d("LoginFragment"," StateView:"+stateViews);
        //Añadir underline a los "hypervinculos"
        SpannableString content = new SpannableString(getString(R.string.registrate_ahora));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        txtRegister.setText(content);
        content = new SpannableString(getString(R.string.cambiar_contrase_a));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        txtChangePsswd.setText(content);
        content = new SpannableString(getString(R.string.olvidaste_tu_cuenta));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        txtForgetPass.setText(content);
        inputLogin(stateViews);
        btnLogin.setText(textButton);
        edPass.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    mListener.OnListenerButtonLogin(edUser.getText().toString(),
                            edPass.getText().toString()
                            ,checkSaveUser.isChecked());
                    return true;
                }
                return false;
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.OnListenerButtonLogin(edUser.getText().toString().trim(),
                        edPass.getText().toString().trim()
                        ,checkSaveUser.isChecked());
            }
        });
        checkSaveUser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
             if(mListener != null) mListener.OnListenerCheckSaveUser(isChecked);
            }
        });
        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null){
                    mListener.OnListenerRegister(0);
                }
            }
        });
        txtForgetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null){
                    mListener.OnListenerRegister(1);
                }
            }
        });
        txtChangePsswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null){
                    mListener.OnListenerRegister(2);
                }
            }
        });
        // Inflate the layout for this fragment
        return view;
    }
    public void inputLogin(boolean state){
        if(edUser != null)  edUser.setEnabled(state);
        if(edPass != null)edPass.setEnabled(state);
        if(edPass != null)edPass.setCursorVisible(state);
        if(edUser != null) edUser.setCursorVisible(state);
        if(txtForgetPass != null)txtForgetPass.setEnabled(state);
       // if(checkSaveUser != null)checkSaveUser.setEnabled(state);
        if(hideEdit != null)hideEdit.setPasswordVisibilityToggleEnabled(state);
        if(hideEdit != null)hideEdit.setEnabled(state);
        stateViews = state;
    }

    // TODO: Rename method, update argument and hook method into UI event

    public void showErrorShakePass() {
        Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.shake);
        edPass.startAnimation(shake);
    }
    public void showErrorShakeUser() {
        Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.shake);
        edUser.startAnimation(shake);
    }
    public void showErrorUser() {
        edUser.setError("Usuario o contraseña incorrecto");
    }
    public void setEdUser(String value){
        textUser = value;
    }
    public void resetInput(){
        edPass.setText("");
        edUser.setText("");
    }
    public void setTextButtonLogin(String text){
        textButton = text;
        if(btnLogin != null) btnLogin.setText(text);
    }
    public String getEdUSer(){
        return edUser.getText().toString();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String title, int num_fragment);
        void OnListenerButtonLogin(String user, String password, boolean saveUser);
        void OnListenerCheckSaveUser(boolean isChecked);
        void OnListenerRegister(int action);
    }
}
