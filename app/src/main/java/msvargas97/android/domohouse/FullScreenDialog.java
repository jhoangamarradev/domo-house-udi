/*
 * Michael Vargas Copyright (c) 2017. - Colombia
 */

package msvargas97.android.domohouse;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * * Created by Michael Vargas on 26/07/2017 3:23 PM
 * File:msvargas97.android.domohouse
 */

public class FullScreenDialog extends DialogFragment implements  View.OnClickListener {

    EditText etNameScenarie;
    ToggleButton btnSun,btnMon,btnTue,btnWed,btnThu,btnFri,btnSat;
    //Crea un array con los botones del dia de la semana
    ToggleButton[] btnDow;
    CheckBox checkDate,checkHourOn,checkHourOff;
    TextView etDateBegin,etDateEnd,etHourOn,etHourOff;
    LinearLayout linearLayoutDate;
    Spinner spinnerSelect;
    Button btnSelect;
    NonScrollListView non_scroll_list;
    OnFragmentInteractionListener  mListener;
    private String title;
    private int num;
    private boolean hourState;
    final static boolean DATE_END = false;
    private boolean dateState=false;
    private boolean new_scenarie=true;
    ScenarieAdapter.Scenarie scenarieLoad = new ScenarieAdapter.Scenarie();;
    ArrayList[] device_name;
    boolean[][] saveCheckStates = new boolean[4][16];
    ArrayList<String> device_select_all = new ArrayList<>();

    public boolean isNew_scenarie() {
        return new_scenarie;
    }
    public void setNew_scenarie(boolean new_scenarie) {
        this.new_scenarie = new_scenarie;
    }

    public void clearList(){
        for(int i=0;i<16;i++){
            saveCheckStates[0][i]=false;
            saveCheckStates[1][i]=false;
            saveCheckStates[2][i]=false;
            saveCheckStates[3][i]=false;
        }
        device_select_all.clear();
        if(spinnerSelect != null)spinnerSelect.setSelection(0);
    }
    public FullScreenDialog() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }


    public void setDevice_name(ArrayList[] device_name) {
        this.device_name = device_name;
    }
    void setScenarieLoad(ScenarieAdapter.Scenarie scenarieLoad){
        this.scenarieLoad = scenarieLoad;
    }
    void setTitleFragment(String title, int num){
        this.title = title;
        this.num = num;
    }
    void updateListView(){
        if(device_name != null) {
            device_select_all = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < device_name[i].size(); j++) {
                    // Log.d(DomoHouseConstants.TAG,"State"+j+":"+saveCheckStates[i][j]);
                    if (saveCheckStates[i][j]) {
                        device_select_all.add((String) device_name[i].get(j));
                        Log.d(DomoHouseConstants.TAG, "Añadido:" + device_name[i].get(j));
                    }
                }
            }
        }
        ArrayAdapter<String>  adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,device_select_all);
        non_scroll_list.setAdapter(adapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fullscreen_dialog, container, false);
        btnSun = (ToggleButton) view.findViewById(R.id.checkDomingo);
        btnMon = (ToggleButton) view.findViewById(R.id.checkLunes);
        btnTue = (ToggleButton) view.findViewById(R.id.checkMartes);
        btnWed = (ToggleButton) view.findViewById(R.id.checkMiercoles);
        btnThu = (ToggleButton) view.findViewById(R.id.checkJueves);
        btnFri = (ToggleButton) view.findViewById(R.id.checkViernes);
        btnSat = (ToggleButton) view.findViewById(R.id.checkSabado);
        etNameScenarie = (EditText) view.findViewById(R.id.etNameScenarie);
        linearLayoutDate = (LinearLayout) view.findViewById(R.id.layoutDate);
        checkDate = (CheckBox) view.findViewById(R.id.checkAllYear);
        checkHourOn = (CheckBox) view.findViewById(R.id.checkHourOn);
        checkHourOff = (CheckBox) view.findViewById(R.id.checkHourOff);
        etDateBegin = (TextView) view.findViewById(R.id.etDateBegin);
        etDateEnd = (TextView) view.findViewById(R.id.etDateEnd);
        etHourOn = (TextView) view.findViewById(R.id.etHourOn);
        etHourOff = (TextView) view.findViewById(R.id.etHourOff);
        spinnerSelect = (Spinner) view.findViewById(R.id.spinOptSelect);
        btnSelect = (Button) view.findViewById(R.id.btnSelect);
        non_scroll_list = (NonScrollListView) view.findViewById(R.id.listSelect);
        btnDow = new ToggleButton[]{btnSun,btnMon,btnTue,btnWed,btnThu,btnFri,btnSat};

        if(mListener != null){
            mListener.onFragmentInteraction(title,num);
        }
        etHourOff.setOnClickListener(this);
        etHourOn.setOnClickListener(this);
        etDateEnd.setOnClickListener(this);
        etDateBegin.setOnClickListener(this);
        btnSelect.setOnClickListener(this);
        etNameScenarie.setMaxEms(32);
        etNameScenarie.setMaxLines(1);

        checkDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    linearLayoutDate.setVisibility(View.GONE);
                }else{
                    linearLayoutDate.setVisibility(View.VISIBLE);
                }
            }
        });
        checkHourOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    etHourOn.setEnabled(isChecked);
                    etHourOn.setClickable(isChecked);
            }
        });
        checkHourOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    etHourOff.setEnabled(isChecked);
                    etHourOff.setClickable(isChecked);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        if(new_scenarie) {
            etNameScenarie.setText(title);
            linearLayoutDate.setVisibility(View.GONE);
            Calendar rightNow = Calendar.getInstance();
            int hour = rightNow.get(Calendar.HOUR_OF_DAY);
            int min = rightNow.get(Calendar.MINUTE);
            checkDate.setChecked(true);
            checkHourOn.setChecked(true);
            checkHourOff.setChecked(true);
            setTimeView(hour, (min < 57) ? min + 2 : min, true);
            setTimeView(hour,(min < 57) ? min + 3 : min+1, false);
            etDateBegin.setText("01/01/2017");
            etDateEnd.setText("31/12/2017");
            for(int i = 0; i < 7;i++) {
                if (i == 0 || i == 6){
                    btnDow[i].setChecked(false);
                }else{
                    btnDow[i].setChecked(true);
                }
            }
        }else{
            etNameScenarie.setText(scenarieLoad.name);
            etHourOn.setText(scenarieLoad.hourOn);
            etHourOff.setText(scenarieLoad.hourOff);
            etDateBegin.setText(scenarieLoad.dateBegin);
            etDateEnd.setText(scenarieLoad.dateEnd);
            for(int i = 0; i < 7;i++) {
                if (scenarieLoad.dow.charAt(i) == '.'){
                    btnDow[i].setChecked(false);
                }else{
                    btnDow[i].setChecked(true);
                }
            }
            if(scenarieLoad.hourOff.contains("null")){
                checkHourOff.setChecked(false);
            }else{
                checkHourOff.setChecked(true);
            }
            if(scenarieLoad.hourOn.contains("null")){
                checkHourOn.setChecked(false);
            }else{
                checkHourOn.setChecked(true);
            }
            if(scenarieLoad.dateBegin.contains("01/01") && scenarieLoad.dateEnd.contains("31/12")){
                linearLayoutDate.setVisibility(View.GONE);
                checkDate.setChecked(true);
            }else{
                linearLayoutDate.setVisibility(View.VISIBLE);
                checkDate.setChecked(false);
            }
        }
        boolean [][] lastChecks= new boolean[4][16];

        if(!new_scenarie){
            for (int j = 0; j < 4;j++) {
                for(int i = 0;i<16;i++) {
                    lastChecks[j][i] = scenarieLoad.stateSaveSelect[j][i];
                   // Log.d(DomoHouseConstants.TAG,"Value"+j+i+":"+lastChecks[j][i]);
                }
            }
        }
        saveCheckStates = lastChecks;
        updateListView();
        super.onStart();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fullscreen_dialog, menu);
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_save:
                if( ((MainActivity)getActivity()).checkedNameScenarie(etNameScenarie.getText().toString()) < 0){
                     Log.d(DomoHouseConstants.TAG,"Escenario no Encontrado");
                }else if (new_scenarie){
                    Log.d(DomoHouseConstants.TAG,"Escenario Encontrado");
                    Toast.makeText(getActivity().getApplicationContext(),"Error:Ya existe un escenario con el mismo nombre",Toast.LENGTH_LONG).show();
                    break;
                }
                boolean empty = true;
                for(int i=0;i<4;i++){
                    for (int j=0;j<16;j++) {
                        if (saveCheckStates[i][j]) {
                            empty = false;
                        }
                    }
                }
                if (etNameScenarie.length() > 32){
                    Toast.makeText(getActivity().getApplicationContext(),"Por favor ingrese un nombre más corto",Toast.LENGTH_LONG).show();
                    break;
                }
                if(empty){
                    Toast.makeText(getActivity().getApplicationContext(),"Por favor seleccione por lo menos un dispositivo a controlar",Toast.LENGTH_LONG).show();
                    break;
                }
                //Log.d(DomoHouseConstants.TAG,"Presiono guardar");
                if(mListener != null){
                    String dow = "";
                    char[] array_dow = new char[]{'s','m','t','w','t','f','s'};
                    for (int i=0;i<btnDow.length;i++){ //Obtiene los dias de la semana seleccionados
                        if(btnDow[i].isChecked()){
                            dow+=array_dow[i];
                        }else dow+=".";
                    }
                    if(dow.contains(".......")){
                        Toast.makeText(getActivity().getApplicationContext(),"Por favor seleccione al menos un dia",Toast.LENGTH_LONG).show();
                        break;
                    }
                    if(checkHourOn.isChecked() || checkHourOff.isChecked()) {

                        String hourOn="null",hourOff="null ";
                        if(checkHourOff.isChecked()){
                            hourOff = etHourOff.getText().toString();
                        }
                        if(checkHourOn.isChecked()){
                            hourOn = etHourOn.getText().toString();
                        }
                        mListener.onSaveButton(etNameScenarie.getText().toString(), dow, etDateBegin.getText().toString(),etDateEnd.getText().toString(),hourOn,hourOff,saveCheckStates);
                        clearList();
                    }else{
                        Toast.makeText(getActivity().getApplication(),"Por favor selecione una de las horas",Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Actualiza la fecha del view {@code etDateEnd}
     * @param year Nuevo A?o
     * @param monthOfYear Nuevo Mes
     * @param dayOfMonth Nuevo d?a
     */
    public void setDateView(int year, int monthOfYear, int dayOfMonth,boolean dateState) {
        String date = String.format("%1$02d/%2$02d/%3$04d",dayOfMonth,monthOfYear,year);
        if (dateState == DATE_END){
            etDateEnd.setText(date);
        }else{
            etDateBegin.setText(date);
        }
        //textFecha.setText(format.format(c.getTime()));
    }
    /**
     * Actualiza la hora del view {@code hora_text}
     * @param hourOfDay Nueva Hora
     * @param minute Nuevos Minutos
     */
    public void setTimeView(int hourOfDay, int minute,boolean hourState) {
        String time = String.format("%1$02d:%2$02d",hourOfDay,minute);
        if(hourState){
          etHourOn.setText(time);
        }else{
            etHourOff.setText(time);
        }
    }
    @Override
    public void onClick(View v) {
        int hour=0,minute=0,day=0,month=0,year=0;
        String[] separated;
        switch (v.getId()){
            case R.id.etHourOn:
                hourState = true;
                if(!etHourOn.getText().toString().contains("null")) {
                    separated = etHourOn.getText().toString().split(":");
                    hour = Integer.parseInt(separated[0]);
                    minute = Integer.parseInt(separated[1]);
                }
                break;
            case R.id.etHourOff:
                if(!etHourOff.getText().toString().contains("null")) {
                    separated = etHourOff.getText().toString().split(":");
                    hour = Integer.parseInt(separated[0]);
                    minute = Integer.parseInt(separated[1]);
                }
                hourState = false;
                break;
            case R.id.btnSelect:
                AlertDialog multichoiceDialog = null;
                //Log.d(DomoHouseConstants.TAG,"Selecciono:"+spinnerSelect.getSelectedItem());
                int pos = 0;
                switch (spinnerSelect.getSelectedItem().toString()){
                    case "Luces": pos = 0;break;
                    case "Ventanas":  pos = 2; break;
                    case "Puertas": pos = 3; break;
                    default: pos = 1; break;
                }
                CharSequence[] items = (CharSequence[]) device_name[pos].toArray(new CharSequence[device_name[pos].size()]);
                final int pos2 = pos;
                final boolean[][] checkStates = new boolean[4][16];
                boolean [][] lastChecks= new boolean[4][16];
                for(int i = 0;i<16;i++) {
                    lastChecks[pos][i] =  saveCheckStates[pos][i];
                    checkStates[pos][i] = saveCheckStates[pos][i];
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(spinnerSelect.getSelectedItem().toString());
                builder.setMultiChoiceItems(items,lastChecks[pos], new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        checkStates[pos2][which] = isChecked;
                    }
                });
                builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        for(int i = 0;i<16;i++) {
                          saveCheckStates[pos2][i] =  checkStates[pos2][i];
                        }
                        updateListView();
                       // Toast.makeText(getActivity().getApplicationContext(),"Lista actualizada",Toast.LENGTH_LONG).show();
                    }
                }).setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                multichoiceDialog = builder.create();
                multichoiceDialog.show();
                break;
        }
        if(v.getId() == R.id.etHourOff || v.getId()==R.id.etHourOn){
            if(etHourOff.getText().toString().contains("null") || etHourOn.getText().toString().contains("null")){
                final Calendar c = Calendar.getInstance();
                hour = c.get(Calendar.HOUR_OF_DAY);
                minute = c.get(Calendar.MINUTE);
            }
            new TimeDialog(new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    setTimeView(hourOfDay,minute,hourState);
                }
            },hour,minute).show(getFragmentManager(), "TimePickerInFull");
            Log.d(DomoHouseConstants.TAG,"H:"+hour+" M:"+minute);
        }
        if(v.getId() == R.id.etDateBegin || v.getId() == R.id.etDateEnd){
            if(v.getId() ==  R.id.etDateEnd){
                dateState = DATE_END;
                separated = etDateEnd.getText().toString().split("/");
            }else{
                dateState = true;
                separated = etDateBegin.getText().toString().split("/");
            }
            day = Integer.parseInt(separated[0]);
            month = Integer.parseInt(separated[1])-1;
            year = Integer.parseInt(separated[2]);
            Log.d(DomoHouseConstants.TAG,"D:"+day+" M:"+month+" y"+year);
            new DateDialog(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    setDateView(year,month+1,dayOfMonth,dateState);
                }
            },day,month,year).show(getFragmentManager(),"DatePickerInFull");
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String title, int num_fragment);
        void onSaveButton(String name,String dow,String dateBegin,String dateEnd,String hourOn,String hourOff,boolean[][] devices_select);
    }
}

