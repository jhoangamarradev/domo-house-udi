/*
 * Michael Vargas Copyright (c) 2017. - Colombia
 */

package msvargas97.android.domohouse;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

/**
 * * Created by Michael Vargas on 26/07/2017 3:58 PM
 * File:msvargas97.android.domohouse
 */
public class DateDialog extends DialogFragment {
    DatePickerDialog.OnDateSetListener listener;
    int year =0;
    int month = 0;
    int day = 0;

    public DateDialog(DatePickerDialog.OnDateSetListener listener, int day, int month, int year) {
        super();
        this.listener = listener;
        this.year =year;
        this.month = month;
        this.day =day;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Obtener fecha actual
        // Retornar en nueva instancia del dialogo selector de fecha
        return new DatePickerDialog(
                getActivity(),
                listener,
                year,
                month,
                day);
    }
}