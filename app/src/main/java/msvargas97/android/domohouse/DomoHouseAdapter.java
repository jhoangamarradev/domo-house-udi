package msvargas97.android.domohouse;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * * Created by Michael Vargas on 23/07/2017 12:58 AM
 * File:msvargas97.android.domohouseapp
 */

public class DomoHouseAdapter extends BaseAdapter {
    Context context;
    int num;
    boolean type_sw;
    int current_pos;
    public boolean ON_OFF = true;
    public boolean CLOSE_OPEN = false;

    ArrayList<String> names = null;
    ArrayList<Integer> states = null;
    AdapterInterface mListener;
    Holder holder;
    public void setmListener(AdapterInterface mListener) {
        this.mListener = mListener;
    }

    DomoHouseAdapter(Context context, boolean type_sw, ArrayList<String> names,ArrayList<Integer> states){
        this.context = context;
        if(names != null) num = names.size();
        this.names = names;
        this.type_sw = type_sw;
        this.states = states;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private static LayoutInflater inflater=null;
    private class Holder
    {
        TextView txtName;
        ImageView imgButton;
    }

    public ArrayList<Integer> getStates() {
        return states;
    }
    public void setStates(ArrayList<Integer> states) {
        this.states = states;

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return num;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    void setStateAll(Integer value){
        for(int i = 0;i<states.size();i++) {
            states.set(i,value);
        }
        notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        for(int pos = 0; pos < states.size();pos++) {
            if (type_sw == ON_OFF) {
                if (states.get(pos) == 1) holder.imgButton.setImageResource(R.drawable.on);
                else holder.imgButton.setImageResource(R.drawable.off);
            } else {
                if (states.get(pos) == 1) holder.imgButton.setImageResource(R.drawable.open);
                else holder.imgButton.setImageResource(R.drawable.close);
            }
        }
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        holder = null;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.control_adapter, parent, false);
            holder.imgButton = (ImageView) convertView.findViewById(R.id.img);
            holder.imgButton.setFocusable(true);
            holder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            holder.txtName.setText(names.get(position));
            if(type_sw == ON_OFF){
                if(states.get(position) == 1) holder.imgButton.setImageResource(R.drawable.on);
                else holder.imgButton.setImageResource(R.drawable.off);
            }else{
                if(states.get(position) == 1) holder.imgButton.setImageResource(R.drawable.open);
                else holder.imgButton.setImageResource(R.drawable.close);
            }
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
          //  holder.txtName.setText(names.get(position));
            notifyDataSetChanged();
        }
        final Holder finalHolder = holder;
        final int pos = position;

        holder.imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(states.get(pos) == 1)states.set(pos,0);
                else states.set(pos,1);
                if(type_sw == ON_OFF){
                    if(states.get(pos) == 1) finalHolder.imgButton.setImageResource(R.drawable.on);
                    else finalHolder.imgButton.setImageResource(R.drawable.off);
                }else{
                    if(states.get(pos)== 1) finalHolder.imgButton.setImageResource(R.drawable.open);
                    else finalHolder.imgButton.setImageResource(R.drawable.close);
                }
                if(mListener != null){//Envia notificación usando la interfaz
                    mListener.OnSwitchListener(pos,states.get(pos));
                }
            }
        });
        return convertView;
    }
    public interface AdapterInterface{
       void OnSwitchListener(int position, Integer state);
    }
}
