package msvargas97.android.domohouse;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ScenariesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ScenariesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScenariesFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private int mParam2;
    FloatingActionButton fabAdd,fabDelete,fabSave;
    ListView listView;
    private OnFragmentInteractionListener mListener;
    ScenarieAdapter scenarieAdapter;
    int which;
    public ScenariesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ScenariesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ScenariesFragment newInstance(String param1, int param2) {
        ScenariesFragment fragment = new ScenariesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_scenaries, container, false);
        if (mListener != null) {
            mListener.onFragmentInteraction(mParam1,mParam2);
        }
        fabAdd = (FloatingActionButton) view.findViewById(R.id.fabAdd);
        fabDelete = (FloatingActionButton) view.findViewById(R.id.fabDelete);
        fabSave = (FloatingActionButton) view.findViewById(R.id.fabSave);
        listView = (ListView) view.findViewById(R.id.listViewScenaries);
        fabDelete.hide();
        fabAdd.setOnClickListener(this);
        fabDelete.setOnClickListener(this);
        fabSave.setOnClickListener(this);
        listView.setFocusable(true);
        listView.setClickable(true);
        listView.setFocusableInTouchMode(true);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                scenarieAdapter.changeStateCheckbox(!scenarieAdapter.isStateCheckBox());
                listView.setAdapter(scenarieAdapter);
                fabDelete.show();
                setHasOptionsMenu(true);
                return false;
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(DomoHouseConstants.TAG,"Se presiono:"+position);
                ScenarieAdapter.Scenarie scenarie = scenarieAdapter.getItem(position);
                if(mListener != null){
                    mListener.OpenFullScreenDialog(scenarie,position);
                }
                which = position;
                Log.d(DomoHouseConstants.TAG,"Nombre:"+scenarie.name);
                Log.d(DomoHouseConstants.TAG,"dow:"+scenarie.dow);
                Log.d(DomoHouseConstants.TAG,"HourOn:"+scenarie.hourOn);
                Log.d(DomoHouseConstants.TAG,"HourOff:"+scenarie.hourOff);
                Log.d(DomoHouseConstants.TAG,"DateBegin:"+scenarie.dateBegin);
                Log.d(DomoHouseConstants.TAG,"DateEnd:"+scenarie.dateEnd);
                Log.d(DomoHouseConstants.TAG,"timer:"+scenarie.timer);
                Log.d(DomoHouseConstants.TAG,"MacroOn:"+scenarie.macros[0]);
                Log.d(DomoHouseConstants.TAG,"MacroOff:"+scenarie.macros[1]);
            }
        });
        return view;
    }

    public int getWhich() {
        return which;
    }

    public void setWhich(int which) {
        this.which = which;
    }

    void setArrays(ArrayList<String> names, ArrayList<String> hoursOff, ArrayList<String> hoursOn, ArrayList<String> dateBegin, ArrayList<String> dateEnd){
        scenarieAdapter.setArrays(names,hoursOn,hoursOff,dateBegin,dateEnd);
        listView.setAdapter(scenarieAdapter);
    }
    void removeListScenarie(){
        scenarieAdapter.removeAllSelect();
        listView.setAdapter(scenarieAdapter);
        actionCancel();
    }
    @Override
    public void onStart() {
        if(scenarieAdapter == null) {
            scenarieAdapter = new ScenarieAdapter(getActivity());
        }
        listView.setAdapter(scenarieAdapter);
        if(mListener != null) mListener.updateScenaries(scenarieAdapter.getCount());
        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fullscreen_dialog, menu);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        fabAdd.setEnabled(false);
        fabSave.setEnabled(false);
        //super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_cancel){
            actionCancel();
        }
        return super.onOptionsItemSelected(item);
    }
    void actionCancel(){
        fabAdd.setEnabled(true);
        fabSave.setEnabled(true);
        fabDelete.hide();
        setHasOptionsMenu(false);
        scenarieAdapter.changeStateCheckbox(!scenarieAdapter.isStateCheckBox());
        listView.setAdapter(scenarieAdapter);
    }
    @Override
    public void onClick(View v) {
        if(mListener != null){
            mListener.OnFabBtnListener(v.getId());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String title, int num_fragment);
        void OnFabBtnListener(int id);
        void OpenFullScreenDialog(ScenarieAdapter.Scenarie scenarieLoad,int which);
        void updateScenaries(int size);
    }
}
