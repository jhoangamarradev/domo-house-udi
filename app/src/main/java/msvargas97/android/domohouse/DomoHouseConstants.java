/*
 * Michael Vargas Copyright (c) 2017. - Colombia
 */

package msvargas97.android.domohouse;

/**
 * * Created by Michael Vargas on 23/07/2017 11:27 PM
 * File:msvargas97.android.domohouse
 */

public interface DomoHouseConstants {
    String PREFS_NAME =  "msvargas97.android.domohouseapp.settings"; //SharedPreferences
    String SERVER_RESULT = "serverResult"; //msg.what para las respuestas del servidor
    String SERVER_REQUEST = "serverRequest";
    String REMEMBER_USER = "saveUser";
    String LAST_USER = "username";
    String LAST_PASSWORD = "password";
    String IP_SERVER = "ip";
    String PORT_SERVER = "port";
    String POS_PORT_SERVER ="pos_port";
    String CONFIG_JSON = "config";
    String TAG = "DomoHouse";
    String MSG_ERROR_SERVER = "Falló la conexión con el servidor!";
    //NUMERO DE FRAGMENTO
    int LOGIN_FRAGMENT = 0;
    int CONTROL_FRAGMENT = 1;
    int SCENARIES_FRAGMENT = 2;
    int REGISTER_FRAGMENT = 3;
    int SETTINGS_FRAGMENT = 4;
    int CONTROL_SW_FRAGMENT = 5;
    int FULL_SCREEN_FRAGMENT = 6;
    int LUCES = 0;
    int ElECTRO = 1;
    int VENTANAS = 2;
    int PUERTAS = 3;
    boolean ON_OFF = true;
    boolean CLOSE_OPEN = false;
}
